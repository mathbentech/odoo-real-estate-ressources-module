# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).


{
    'name': 'Real Estate Agent',
    'version': '0.1',
    'category': 'Stock',
    'description': """
Implement real estate agent and more
====================================

TODO
""",
    'depends': ['base'],
    'data': [
        'views/partner_real_estate_view.xml',
    ],
    'installable': True,
    'post_init_hook': 'load_translations',
}
