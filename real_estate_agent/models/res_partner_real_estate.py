# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import _, api, exceptions, fields, models


class ResPartnerRealEstate(models.Model):
    _inherit = 'res.partner'

    # General
    need_approval = fields.Boolean(string='Need approval',
                                   help="Check this box to flag need approval.")

    # Profession
    appraiser = fields.Boolean(string='Is an appraiser',
                               help="Check this box if this contact is an appraiser.")

    agent = fields.Boolean(string='Is an agent',
                           help="Check this box if this contact is an agent.")

    broker = fields.Boolean(string='Is a broker',
                            help="Check this box if this contact is a broker.")

    contractor = fields.Boolean(string='Is a contractor',
                                help="Check this box if this contact is a contractor.")

    designer = fields.Boolean(string='Is a designer',
                              help="Check this box if this contact is a designer.")

    electrician = fields.Boolean(string='Is an electrician',
                                 help="Check this box if this contact is an electrician.")

    general_contractor = fields.Boolean(string='Is a general contractor',
                                        help="Check this box if this contact is a general contractor.")

    inspector = fields.Boolean(string='Is an inspector',
                               help="Check this box if this contact is an inspector.")

    insurance_broker = fields.Boolean(string='Is an insurance broker',
                                      help="Check this box if this contact is an insurance broker.")

    kitchen_cabinetry_contractor = fields.Boolean(string='Is a kitchen cabinetry contractor',
                                                  help="Check this box if this contact is a kitchen cabinetry "
                                                       "contractor.")

    mover = fields.Boolean(string='Is a mover',
                           help="Check this box if this contact is a mover.")

    notary = fields.Boolean(string='Is a notary',
                            help="Check this box if this contact is a notary.")

    photographer = fields.Boolean(string='Is a photographer',
                                  help="Check this box if this contact is a photographer.")

    plumber = fields.Boolean(string='Is a plumber',
                             help="Check this box if this contact is a plumber.")

    surveyor = fields.Boolean(string='Is a surveyor',
                              help="Check this box if this contact is a surveyor.")

    well_and_septic_contractor = fields.Boolean(string='Is a well and septic contractor',
                                                help="Check this box if this contact is a well and septic contractor.")

    # Fields for profession
    license_number = fields.Char(string='License Number', help="License number of agent.")
